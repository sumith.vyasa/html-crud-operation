from django.db import models

# Create your models here.
class book(models.Model):
    name = models.CharField(max_length = 50)
    authors = models.CharField(max_length=50,default='author_default')
    picture = models.ImageField()
    email = models.EmailField(blank = True)
    describe = models.TextField()
    #author  = models.ForeignKey('author', on_delete=models.CASCADE)
    def __str__(self):
        return self.name

class author(models.Model):
    class meta:
        verbose_name_plural = 'authors'
    name =models.CharField(max_length=30)
    title = models.TextField(max_length =200,default='default title')
    email = models.EmailField

    def __str__(self):
        return self.name
