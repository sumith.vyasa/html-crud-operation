from django import forms
from .models import book
from .models import author

class BookCreate(forms.ModelForm):
    class Meta:
        model = book
        fields = '__all__'


class author(forms.ModelForm):
    class Meta:
        model = author
        fields =  '__all__'